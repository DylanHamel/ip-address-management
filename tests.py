#!/usr/bin/env python3.7

import IPAddress

#
#
#
def main() :

    ipAddressMask = "192.168.1.200 255.255.255.255"
    ipAddressMaskCIDR = "192.168.1.3/30"

    print(IPAddress.extractNetmaskCIDR(ipAddressMaskCIDR))
    print(IPAddress.extractIPAddressWithNetmaskCIDR(ipAddressMaskCIDR))
    
    print(IPAddress.convertCIDRToNetmask("24"))
    print(IPAddress.convertBitInIPAddress(
        IPAddress.convertCIDRToNetmask("24")))

    print(IPAddress.isBroadcastAddressMaskCIDR(ipAddressMaskCIDR))

    print(IPAddress.isBroadcastAddressCIDR("192.168.1.3", "30"))

    return

    ip = "192.168.1.3"
    mask = "255.255.255.252"

    print("BROADCAST = ", IPAddress.isBroadcastAddress(ip, mask))
    print("BROADCAST = ", IPAddress.isBroadcastAddressMask(ipAddressMask))

    return

    IPAddress.generateRandomIPAddressInSubnetBothWithAsk(ip, mask)

    ipAddress = IPAddress.extractIPAddress(ipAddressMask)
    ipAddress10 = "10.0.0.1"
    ipAddress192 = "192.168.1.2"
    netmask = IPAddress.extractNetmaskFromIPAddress(ipAddressMask)
    cidrMask = IPAddress.convertNetmaskToCIDR(netmask)

    print("[tests - main] ipAddress -", ipAddress)
    print("[tests - main] netmask   -", netmask)
    print("[tests - main] cidrMask  -", cidrMask)

    print("[tests - main] is in Same subnet  -",
          ipAddress192, "and", ipAddressMask, "=", IPAddress.isInSameSubnet(
              ipAddress192, ipAddressMask))

    print("[tests - main] is in Same subnet  -",
          ipAddress10, "and", ipAddressMask, "=", IPAddress.isInSameSubnet(
              ipAddress10, ipAddressMask))
    count = 0
    listIP = list()
    listIPSeparate = list()
    while (count < 10):
        listIP.append(
            IPAddress.generateRandomIPAddressInSubnet(ipAddressMask))
        listIPSeparate.append(
            IPAddress.generateRandomIPAddressInSubnetBoth(ip, mask))
        count = count + 1
        
    print("[tests - main] listIP  -", listIP)
    print("[tests - main] listIP  -", listIPSeparate)

# --------------------------------------------------------------------------------------------------
#
#
#
if __name__ == "__main__":
   main()
