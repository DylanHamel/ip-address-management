# IPv4 Addresses Management

This file manage IPv4 addresses. You have the following functions :

* Generate a new IP address in a network

  Parametre can be 

  * ```generateRandomIPAddressInSubnetBoth (ipAddress, netmask)```
    * ipAddress = "192.168.1.1"
    * netmask = "255.255.255.0"

  * ```generateRandomIPAddressInSubnet (ipAddressWithMask)```
    * ipAddressWithMask = "192.168.1.1 255.255.255.0"

* Check if an IP address is in the same subnet that an other

  * ```isInSameSubnet (ipAddress, ipAddressWithMask)```

    * ipAddress = "192.168.1.222"
    * ipAddressWithMask = "192.168.1.1 255.255.255.0"

    Check if ```192.168.1.222``` is in the same subnet than ```192.168.1.1 255.255.255.0```

* Check if mask is valid
  * ```isValideNetmask(netmask)```
    * netmask = "255.255.255.224"

* ```convertBitInIPAddress (IPAddressInBits)```

* ```convertInBitFormat (data)```

* ```convertNetmaskToCIDR(netmask)```
  * netmask = "255.255.255.0"
  * return "24"

* ```extractNetmaskFromIPAddress(ipv4WithNetmask)```
  * ipAddressWithMask = "192.168.1.1 255.255.255.0"
  * return "255.255.255.0"

* ```extractIPAddress(ipv4WithNetmask) ```
  * ipAddressWithMask = "192.168.1.1 255.255.255.0"
  * return "192.168.1.1"

* ```isBroadcastIPAddress(ipAddress, netmask)```

  * Parameter
    * ```(ipAddress, netmask)``` = "192.168.1.255", "255.255.255.0"
    * ```(ipAddress, netmaskCIDR)```  = "192.168.1.255", "24"
    * ```(ipAddressWithMaskCIDR)``` = "192.168.1.255/24"
    * ```(ipAddressWithMask)``` = "192.168.1.255 255.255.255.0"



You can run the script ```./tests.py``` for have some examples



## In Progress

* ```isSubnetIPAddress(ipAddress, netmask)```

* ```generateRandomIPAddressInSubnetBothCIDR (ipAddress, netmaskCIDR)```
* ```isInSameSubnet (ipAddress, ipAddressWithMaskCIDR)```

