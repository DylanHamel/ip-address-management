import ipaddress
import random
import time
# NEW CLASS FOR CONVERT NETMASK AND MGMT IP ADDRESS


def isBroadcastAddressCIDR(ipAddress, netmaskCIDR):

    print("[isBroadcastAddressCIDR] ipAddress     - |", ipAddress, "|")
    print("[isBroadcastAddressCIDR] netmaskCIDR   - |", netmaskCIDR, "|")

    netmask = convertBitInIPAddress(convertCIDRToNetmask(netmaskCIDR))

    print("[isBroadcastAddressMaskCIDR] netmask   - |", netmask, "|")

    return checkBroadcast(ipAddress, netmask)

def isBroadcastAddressMaskCIDR(ipAddressWithMaskCIDR):

    print("[isBroadcastAddressMaskCIDR] receive ipAddressWithMaskCIDR - |",
          ipAddressWithMaskCIDR, "|")

    netmaskCIDR = extractNetmaskCIDR(ipAddressWithMaskCIDR)
    ipAddress = extractIPAddressWithNetmaskCIDR(ipAddressWithMaskCIDR)

    print("[isBroadcastAddressMaskCIDR] ipAddress     - |", ipAddress, "|")
    print("[isBroadcastAddressMaskCIDR] netmaskCIDR   - |", netmaskCIDR, "|")

    netmask = convertBitInIPAddress(convertCIDRToNetmask(netmaskCIDR))

    print("[isBroadcastAddressMaskCIDR] netmask   - |", netmask, "|")

    return checkBroadcast(ipAddress, netmask)

def isBroadcastAddressMask(ipAddressWithMask):
    
    print("[isBroadcastAddress] receive ipAddressWithMask - |",
          ipAddressWithMask, "|")

    ipAddress = extractIPAddress(ipAddressWithMask)
    netmask = extractNetmaskFromIPAddress(ipAddressWithMask)

    print("[isBroadcastAddress] ipAddress - |", ipAddress, "|")
    print("[isBroadcastAddress] netmask   - |", netmask, "|")

    return checkBroadcast(ipAddress, netmask)


def isBroadcastAddress(ipAddress, netmask):

    print("[isBroadcastAddress] receive ipAddress       - |", ipAddress, "|")
    print("[isBroadcastAddress] receive netmask         - |", netmask, "|")

    return checkBroadcast(ipAddress,netmask)
    
    
def checkBroadcast(ipAddress, netmask):

    assert ipaddress.IPv4Address(ipAddress)
    assert isValideNetmask(netmask)

    ipAddressInBits = convertInBitFormat(ipAddress)
    netmaskInBits = convertInBitFormat(netmask)

    print("[isBroadcastAddress] receive ipAddressInBits - |", ipAddressInBits, "|")
    print("[isBroadcastAddress] receive netmaskInBits   - |", netmaskInBits, "|")
    
    indexOfLast1 = (netmaskInBits.find("0"))

    print("[checkBroadcast] netmaskInBitsINDEX - |",
          netmaskInBits[indexOfLast1:], "|")
    print("[checkBroadcast] ipAddressInBitsINDEX - |",
          ipAddressInBits[indexOfLast1:], "|")

    for bit in enumerate(ipAddressInBits[indexOfLast1:]):
        if "0" in bit:
            return False
    return True



def generateIPAddress(ipAddressBytes, netmaskBytes, ipAddressWithMask):

    assert ipaddress.IPv4Address(ipAddressBytes)
    assert isValideNetmask(netmaskBytes)

    ipAddressBits = convertInBitFormat(ipAddressBytes)
    netmaskBits = convertInBitFormat(netmaskBytes)

    print("[generateIPAddress] ipAddressBits - |", ipAddressBits, "|")
    print("[generateIPAddress] netmaskBits - |", netmaskBits, "|")

    indexOfLast1 = (netmaskBits.find("0"))
    hostPartOfIPAddress = ipAddressBits[indexOfLast1:]
    lengthHostPartOfIPAddress = hostPartOfIPAddress.__len__()

    print("[generateIPAddress] hostPartOfIPAddress - |",
          hostPartOfIPAddress, "|")
    print("[generateIPAddress] lengthHostPartOfIPAddress - |",
          lengthHostPartOfIPAddress, "|")

    # we generate a random number. This number is index of bit will be changed
    indexOfBitWillBeChanged = (random.randint(1, lengthHostPartOfIPAddress))-1
    listIPAddressBits = list(ipAddressBits)

    indexOfBitWillBeChanged = 1 if indexOfBitWillBeChanged == 0 else indexOfBitWillBeChanged

    if listIPAddressBits[(indexOfLast1+indexOfBitWillBeChanged-1)] == "1" :
        listIPAddressBits[(indexOfLast1+indexOfBitWillBeChanged-1)] = "0"
    else :
        listIPAddressBits[(indexOfLast1+indexOfBitWillBeChanged-1)] = "1"

    ipAddressBits = ''.join(listIPAddressBits)

    print("[generateIPAddress] ipAddressBits(2) - |",
          ipAddressBits, "|")
    
    ip = convertBitInIPAddress(ipAddressBits)
    isInSameNetwork = isInSameSubnet(ip, ipAddressWithMask)
    print("[generateIPAddress] is in same subnet ?  - |",
          isInSameNetwork, "|")
    assert isInSameNetwork
    print("[generateIPAddress] Return  - |", ip, "|")

    return ip



def generateRandomIPAddressInSubnetBothWithAsk (ipAddress, netmask) :
    """

    :param ipAddress:
    :param netmask:
    :return:
    """
    
    print("[generateRandomIPAddressInSubnetBothWithAsk] Receive - |", ipAddress, "| and |", netmask, "|")
    ipAddressWithMask = ipAddress + " " + netmask
    print("[generateRandomIPAddressInSubnetBoth] ipAddressWithMask - |",
          ipAddressWithMask, "|")

    ip = ""
    while (True):
        ip = generateIPAddress(ipAddress, netmask, ipAddressWithMask)
        ok = input(
            "[generateRandomIPAddressInSubnetBothWithAsk] - This IP (" + ip + ") is Ok for you ? [y=yes/n=NoAndEnterManually]")
         
        while ok is not "y" and ok is not "n":
            ok = input("Please answer by [y/n]")

        if ok == "y":
            return ip
#
#
#
#
def generateRandomIPAddressInSubnetBoth (ipAddress, netmask) :
    """

    :param ipAddress:
    :param netmask:
    :return:
    """
    
    print("[generateRandomIPAddressInSubnetBoth] Receive - |", ipAddress, "| and |",netmask, "|")
    ipAddressWithMask = ipAddress + " " + netmask
    print("[generateRandomIPAddressInSubnetBoth] ipAddressWithMask - |",
          ipAddressWithMask, "|")

    return generateIPAddress(ipAddress, netmask, ipAddressWithMask)

    

def generateRandomIPAddressInSubnet (ipAddressWithMask) :

    """
    :param ipAddressWithMask: a String as "192.168.10.11 255.255.255.128"
    :return: ipAddress without netmask in same subnet as ip address give in parameter
    """
    
    print("[generateRandomIPAddressInSubnet] Receive - ", ipAddressWithMask, "|")

    netmaskBytes = extractNetmaskFromIPAddress(ipAddressWithMask)
    ipAddressBytes = extractIPAddress(ipAddressWithMask)

    print("[generateRandomIPAddressInSubnet] netmaskBytes - ", netmaskBytes, "|")
    print("[generateRandomIPAddressInSubnet] ipAddressBytes - ", ipAddressBytes, "|")

    return generateIPAddress(ipAddressBytes, netmaskBytes, ipAddressWithMask)

    
#
#
#
#
def isInSameSubnet (ipAddress, ipAddressWithMask) :
    """
    :param ipAddress: a String as "192.168.10.200
    :param ipAddressWithMask: a String as "192.168.10.11 255.255.255.128"
    :return:
    """

    print("[isInSameSubnet] Receive - ", ipAddress, " and ", ipAddressWithMask)
    assert ipaddress.IPv4Address(ipAddress)

    isInSame = True
    netmaskBytes = extractNetmaskFromIPAddress(ipAddressWithMask)
    ipAddressBytes = extractIPAddress(ipAddressWithMask)

    print("[isInSameSubnet] - |", ipAddressBytes, "|")
    print("[isInSameSubnet] - |", netmaskBytes, "|")

    assert ipaddress.IPv4Address(ipAddressBytes)
    assert isValideNetmask(netmaskBytes)


    ipAddressInBits = convertInBitFormat(ipAddress)
    ipAddressBits = convertInBitFormat(ipAddressBytes)
    netmaskBits = convertInBitFormat(netmaskBytes)

    print("[isInSameSubnet] ipAddressInBits - |",ipAddressInBits,"|")
    print("[isInSameSubnet] ipAddressBits   - |",ipAddressBits,"|")
    print("[isInSameSubnet] netmaskBits     - |",netmaskBits,"|")

    indexOfLast1 = (netmaskBits.find("0"))

    only1InNetmask = netmaskBits[:indexOfLast1]

    index = 0
    while index < indexOfLast1 and isInSame:
        if ipAddressInBits[index] != ipAddressBits[index] :
            print(ipAddressInBits[index], " == ", ipAddressBits[index], "?")
            isInSame = False

        index = index + 1

    print("[isInSameSubnet] Return  - ", isInSame, "|")
    return isInSame



def isValideNetmask(netmask):
    """
    We receive a netmask.
    We check if is it compose by 4 bytes
    Check if is it a correct mask and return it if it's correct

    :param netmask:
    :return int: -1 if netmask is incorrect and CIDR notation Lenght
    """

    # We check if length netmask is 32 (4 bytes)
    print("[isValideNetmask] Receive -" , netmask, "|")
    netmaskBits = convertInBitFormat(netmask)
    assert netmaskBits.__len__() == 32

    # We check if netmask is valid
    indexFirst0 = netmaskBits.find("0")
    if netmaskBits.find("0") != -1:
        if netmaskBits[indexFirst0:].find("1") != -1:
            print("[isValideNetmask] Return  -", False, "|")
            return False

    print("[isValideNetmask] Return  -", True, "|")
    return True
#
#
#
#
def convertBitInIPAddress (IPAddressInBits) :
    """

    :param IPAddressInBits:
    :return:
    """

    print("[convertBitInIPAddress] Receive - ", IPAddressInBits, "|")
    print("[convertBitInIPAddress] Return  - ", \
          str(int(IPAddressInBits[:8], 2)) + "." + str(int(IPAddressInBits[8:16], 2)) + \
          "." + str(int(IPAddressInBits[16:24], 2)) + "." + str(int(IPAddressInBits[24:], 2)), "|")

    return ("" + str(int(IPAddressInBits[:8], 2)) + "." + str(int(IPAddressInBits[8:16], 2)) + \
            "." + str(int(IPAddressInBits[16:24], 2)) + "." + str(int(IPAddressInBits[24:], 2)))

#
#
#
#
def convertInBitFormat (data):
    """
    :param netmask:
    :return:
    """
    
    # We put in a list 4 bytes and verify there are exactly 4 bytes
    allBytes = data.split(".")

    print("[convertInBitFormat] Receive  - ", data, "|")
    print("[convertInBitFormat] allBytes -", allBytes, "|")
    assert allBytes.__len__() == 4

    # We put in a string the netmask in bits
    netmaskCIDR = ""
    for bytes in allBytes:
        # [2:] for remove "0b"
        netmaskCIDR = netmaskCIDR + str(bin(int(bytes))[2:].zfill(8))

    print("[convertInBitFormat] Return   - ", netmaskCIDR, "|")
    return netmaskCIDR

#
#
#
#
def convertNetmaskToCIDR(netmask) :
    """

    :param netmask:
    :return:  Netmask length CIDR Notation. It's only "how many 1 there are"
    """
    
    print("[convertNetmaskToCIDR] Receive - ", netmask, "|")
    if isValideNetmask(netmask) :
        netmaskBits = convertInBitFormat(netmask)
        netmaskNotationCIDR = netmaskBits.find("0")
        print("[convertNetmaskToCIDR] Return  - ", netmaskNotationCIDR, "|")
        return netmaskNotationCIDR
#
#
#
#
def convertCIDRToNetmask(netmaskCIDR):
    """

    :param netmaskCIDR notation:
    :return:  Netmask in 4 bytes format 255.255.255.0
    """

    print("[convertNetmaskToCIDR] Receive - ", netmaskCIDR, "|")
    count = 0
    netmask = ""
    while (count < int(netmaskCIDR)):
       netmask = netmask + "1"
       count = count + 1

    print(netmask.__len__(), " - ", netmask)

    while (netmask.__len__() < 32):
        netmask = netmask + "0"
        print(netmask.__len__())

    return netmask
#
#
#
#
def extractNetmaskFromIPAddress(ipv4WithNetmask) :
    """
    :param ipv4WithNetmask:
    :return
    """

    # We receive in parameter an ipv4 address and mask as "192.168.10.11 255.255.255.0"

    # We will extract only the netmask from ip parameter
    # There is not check in this function
    
    print("[extractNetmaskFromIPAddress] Receive - ", ipv4WithNetmask, "|")
    indexNetmask = (ipv4WithNetmask.find(" ") + 1)
    netmask = ipv4WithNetmask[indexNetmask:]

    print("[extractNetmaskFromIPAddress] Return  - ", netmask, "|")
    return netmask
#
#
#
#
def extractIPAddress(ipv4WithNetmask) :
    """
    :param ipv4WithNetmask:
    :return
    """

    # We receive in parameter an ipv4 address and mask as "192.168.10.11 255.255.255.0"

    # We will extract only the ip address
    # There is not check in this function only separation
    
    print("[extractIPAddress] Receive - ", ipv4WithNetmask, "|")

    indexEndIPv4Address = (ipv4WithNetmask.find(" "))
    ipAddres = ipv4WithNetmask[:indexEndIPv4Address]

    print("[extractIPAddress] Return  - ", ipAddres, "|")
    return ipAddres
#
#
#
#
def extractNetmaskCIDR(ipv4WithNetmaskCIDR):
    """
    :param ipv4WithNetmaskCIDR:
    :return mask in CIDR notation
    """

    # We receive in parameter an ipv4 address and mask as "192.168.10.11/24"

    # We will extract only CIDR Mask
    # There is not check in this function only separation

    print("[extractIPAddress] Receive - ", ipv4WithNetmaskCIDR, "|")

    indexEndIPv4Address = (ipv4WithNetmaskCIDR.find("/"))
    netmaskCIDR = ipv4WithNetmaskCIDR[indexEndIPv4Address+1:]

    print("[extractIPAddress] Return  - ", netmaskCIDR, "|")
    return netmaskCIDR
#
#
#
#
def extractIPAddressWithNetmaskCIDR(ipv4WithNetmaskCIDR):
    """
    :param ipv4WithNetmaskCIDR:
    :return mask in CIDR notation
    """

    # We receive in parameter an ipv4 address and mask as "192.168.10.11/24"

    # We will extract only ip address
    # There is not check in this function only separation

    print("[extractIPAddress] Receive - ", ipv4WithNetmaskCIDR, "|")

    indexEndIPv4Address = (ipv4WithNetmaskCIDR.find("/"))
    netmaskCIDR = ipv4WithNetmaskCIDR[:indexEndIPv4Address]

    print("[extractIPAddress] Return  - ", netmaskCIDR, "|")
    return netmaskCIDR
#
#
#
#
